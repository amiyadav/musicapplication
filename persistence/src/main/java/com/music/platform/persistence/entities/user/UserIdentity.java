	package com.music.platform.persistence.entities.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.AuditableEntity;

@Entity
@NamedQueries({
    @NamedQuery(name = "UserIdentity.getValidatedUserIdentity",
            query = "Select userIdentity from UserIdentity as userIdentity "
            + "where userIdentity.userName = :userName "
            + "and userIdentity.password = :password")
})
@Table(name = "USER_IDENTITY")
public class UserIdentity extends AuditableEntity<UserMaster> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8469910309710130317L;

	@Id
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "USER_MASTER_USER_ID")
	private UserMaster userMaster;
	
	@Column(name = "USERNAME")
	private String userName;
	
	@Column(name = "PASSWORD")
	private String password;
	
	@Column(name = "IS_ACTIVE")
	private Boolean isActive;
	

	@Override
	public UserMaster getId() {
		// TODO Auto-generated method stub
		return userMaster;
	}
	
	public void setId(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	@Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.userName);
		list.add(this.isActive);
		return list;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}
