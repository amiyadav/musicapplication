package com.music.platform.persistence.entities.user;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.AuditableEntity;
import com.music.platform.persistence.entities.subscription.SubscriptionItems;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "USER_MASTER")
public class UserMaster extends AuditableEntity<Long> implements Serializable {
	public UserMaster() {

	}

	public UserMaster(UserMaster userMaster) {

	}

	@Id
	@Column(name = "USER_ID", updatable = false, nullable = false)
	@GeneratedValue
	private Long userId;

	@Column(name = "FNAME")
	private String fName;

	@Column(name = "MNAME")
	private String mName;

	@Column(name = "LNAME")
	private String lName;

	@Column(name = "TITLE")
	private String title;

	@Column(name = "EMAIL1")
	private String email1;

	@Column(name = "MOBILE1")
	private String mobile1;

	@Column(name = "DOB")
	private Date DOB;

	@Column(name = "PROFILE_PICTURE_PATH")
	private String profilePicturePath;

	@Column(name = "GENDER", columnDefinition = "enum('MALE', 'FEMALE', 'OTHERS')")
	private String gender;

	@ManyToOne
	@JoinColumn(name = "USER_TYPE_USER_TYPE_ID", referencedColumnName = "USER_TYPE_ID")
	private UserType userType;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "USER_CONTENT_SINGER", joinColumns = {
			@JoinColumn(name = "USER_MASTER_USER_ID") }, inverseJoinColumns = {
					@JoinColumn(name = "CONTENT_CONTENT_ID") })
	private Set<SubscriptionItems> subscriptionItems;
	
	@Override
	public Long getId() {
		return userId;
	}

	@Override
	public List<? extends Object> getCompareables() {
		List<Object> list = new ArrayList<Object>();
		list.add(this.userId);
		list.add(this.userType);
		return list;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getMobile1() {
		return mobile1;
	}

	public void setMobile1(String mobile1) {
		this.mobile1 = mobile1;
	}

	public Date getDOB() {
		return DOB;
	}

	public void setDOB(Date dOB) {
		DOB = dOB;
	}

	public String getProfilePicturePath() {
		return profilePicturePath;
	}

	public void setProfilePicturePath(String profilePicturePath) {
		this.profilePicturePath = profilePicturePath;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

}
