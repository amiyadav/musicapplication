package com.music.platform.persistence.entities.user;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.swing.text.AbstractDocument.Content;

import com.music.platform.persistence.entities.baseentity.CompositeKey;

public class ClientContentPK extends CompositeKey {

	@ManyToOne
	@JoinColumn(name = "CLIENT_USER_MASTER_USER_ID")
	private Client client;
	
	@ManyToOne
	@JoinColumn(name = "CONTENT_CONTENT_ID")
	private Content content;
	
	@Override
	public List<? extends Object> getCompareables() {
		List<Object> list = new ArrayList<Object>();
		list.add(this.client);
		list.add(this.content);
		return list;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Content getContent() {
		return content;
	}

	public void setContent(Content content) {
		this.content = content;
	}

}
