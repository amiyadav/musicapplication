package com.music.platform.persistence.entities.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.AuditableEntity;

@Entity
@Table(name = "CLIENT_STATS")
public class ClientStats extends AuditableEntity<Client> implements Serializable{

	private static final long serialVersionUID = -8485861599278288171L;
	@Id
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CLIENT_USER_MASTER_USER_ID")
	private Client client;
	
	@Override
	public Client getId() {
		return client;
	}

	@Override
	public List<? extends Object> getCompareables() {
		List<Object> list = new ArrayList<Object>();
		list.add(this.client);
		
		return list;
	}
	
	public void setClient(Client client) {
		this.client = client;
	}

}
