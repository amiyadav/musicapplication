package com.music.platform.persistence.entities.baseentity;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@MappedSuperclass
public abstract class AuditableEntity<ID extends Serializable> extends DataAccessEntity<ID> {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "aud_created_date")
    private Date creationDate;

    @Column(name = "aud_updated_date")
    private Date updatedDate;

    /**
     * Get the creation date of the entity.
     * 
     * @return the creation date
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * Set the creation date of the entity.
     * 
     * @param creationDate
     *            the creation date
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Get the date when the entity was updated last time.
     * 
     * @return the last update date
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getUpdatedDate() {
        return updatedDate;
    }

    /**
     * Set the date when the entity was updated last time.
     * 
     * @param updatedDate
     *            the last update date
     */
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

}
