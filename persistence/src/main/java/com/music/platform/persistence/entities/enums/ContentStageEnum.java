package com.music.platform.persistence.entities.enums;

public enum ContentStageEnum {

	READY("READY"),
	UPLOADED("UPLOADED");
	
	public final String contentStage;

	ContentStageEnum(String contentStage) {
		this.contentStage = contentStage;
	}
	public String getContentStage() {
		return contentStage;
	}
	
}
