package com.music.platform.persistence.entities.subscription;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.music.platform.persistence.entities.user.UserMaster;

@Entity
@Table(name="SUBSCRIPTION_ITEMS")
public class SubscriptionItems implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="SUBSCRIPTION_ITEMS_ID")
	private Integer subscriptionItemsId;
	
	@ManyToOne
	@Column(name="SUBSCRIPTIONS_SUBSCRIPTIONS_ID")
	private Subscriptions subscriptions;
	
	@ManyToOne
	@Column(name="CREATOR_ID")
	private UserMaster usermaster;
	
	@Column(name="IS_ACTIVE")
	private Boolean isActive;
	
	@Column(name="START_DATE")
	private Date startDate;
	
	@Column(name="END_DATE")
	private Date endDate;
	
	@ManyToOne
	@Column(name="SUBSCRIPTION_TYPE_SUBSCRIPTION_TYPE_ID")
	private SubscriptionType subscriptionType;


//	@Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.subscriptionItemsId);
		list.add(this.subscriptions);
		list.add(this.usermaster);
		list.add(subscriptionType);
		return list;
		
	}
	
	public Integer getSubscriptionItemsId() {
		return subscriptionItemsId;
	}

	public void setSubscriptionItemsId(Integer subscriptionItemsId) {
		this.subscriptionItemsId = subscriptionItemsId;
	}

	public Subscriptions getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(Subscriptions subscriptions) {
		this.subscriptions = subscriptions;
	}

	public UserMaster getUsermaster() {
		return usermaster;
	}

	public void setUsermaster(UserMaster usermaster) {
		this.usermaster = usermaster;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public SubscriptionType getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(SubscriptionType subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
