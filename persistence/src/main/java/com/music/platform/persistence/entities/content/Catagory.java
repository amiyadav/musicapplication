package com.music.platform.persistence.entities.content;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.DataAccessEntity;
import com.music.platform.persistence.entities.enums.*;

@Entity
@Table(name = "CATEGORY")
public class Catagory extends DataAccessEntity<CategoryTypeEnum>{

	private static final long serialVersionUID = 1L;

	@Id
	@Enumerated(EnumType.STRING)
	@Column(name = "CATAGORY_ID", updatable = false, nullable = false)
	private String catagoryId;
	
	@Column(name = "DESCRIPTION")
	private String description;

	public String getCatagoryId() {
		return catagoryId;
	}

	public void setCatagoryId(String catagoryId) {
		this.catagoryId = catagoryId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.catagoryId);
		list.add(this.description);
		
		return list;
	}

	@Override
	public CategoryTypeEnum getId() {
	// TODO Auto-generated method stub
	return null;
}

}
