package com.music.platform.persistence.entities.order;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "ORDER_STATUS")
public class OrderStatus {
	@Id
	@Column(name = "ORDER_STATUS_ID", updatable = false, nullable = false)
	@GeneratedValue
	private Long orderID;
	
	@ManyToOne
	@JoinColumn(name = "ORDER_ORDER__ID", referencedColumnName = "ORDER_ID")
	private Order order;

	@Column(name="DESCRIPTION")
	private String description;

	public Long getOrderID() {
		return orderID;
	}

	public void setOrderID(Long orderID) {
		this.orderID = orderID;
	}

	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}
	
}
