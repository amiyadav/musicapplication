package com.music.platform.persistence.entities.content;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.DataAccessEntity;
import com.music.platform.persistence.entities.enums.ContentStageEnum;

@Entity
@Table(name="CONTENT_STAGE")
public class ContentStage extends DataAccessEntity<ContentStageEnum> {

	private static final long serialVersionUID = 1L;

	@Id
	@Enumerated(EnumType.STRING)
	@Column(name="CONTENT_STAGE_ID", updatable = false, nullable = false)
	private ContentStageEnum contentStageId;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ContentStageEnum getContentStageId() {
		return contentStageId;
	}

	public void setContentStageId(ContentStageEnum contentStageId) {
		this.contentStageId = contentStageId;
	}
	
	@Override
	public ContentStageEnum getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.contentStageId);
		list.add(this.description);
		return list;
	}

}
