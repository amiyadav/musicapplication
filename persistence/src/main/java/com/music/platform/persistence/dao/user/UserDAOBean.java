package com.music.platform.persistence.dao.user;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.music.platform.persistence.dao.basedao.bean.BaseDAOBean;
import com.music.platform.persistence.entities.user.UserIdentity;


@Transactional
public class UserDAOBean extends BaseDAOBean{
	
    
    public SessionFactory getSessionFactory(){
    	
    	return sessionFactory;
    }

    public Session getSession(){
    	Session session = getSessionFactory().getCurrentSession();    
    	//Filter filter = (Filter) session.enableFilter("archivedFlagFilter");
    	return  session;
    
    }
    @SuppressWarnings("unchecked")
	public List<UserIdentity> getValidatedUserIdentity( @NotNull String userName, 
    											 		@NotNull String password){
    	System.out.println("dao: " + userName);
    	Query query = getSession().getNamedQuery("UserIdentity.getValidatedUserIdentity").setString("userName", userName)
    																					 .setString("password", password);
    	
    	
    	return query.list();

    }
//    @SuppressWarnings("unchecked")
//	public List<UserMaster> getUserMaster( @NotNull long userId){
//    	System.out.println("dao: " + userId);
//    	Query query = getSession().getNamedQuery("UserMaster.getUserMaster").setLong("userId", userId);
//    	
//    	return query.list();
//
//    }
}