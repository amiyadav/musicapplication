package com.music.platform.persistence.entities.user;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.DataAccessEntity;

@Entity
@Table(name = "MIXER")
public class Mixer extends DataAccessEntity<UserMaster> {
	@Id
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "USER_MASTER_USER_ID")
	private UserMaster userMaster;

	@Column(name="RATING")
	private int rating;
	
	@Override
	public UserMaster getId() {
		// TODO Auto-generated method stub
		return userMaster;
	}

	@Override
	public List<? extends Object> getCompareables() {
		List<Object> list = new ArrayList<Object>();
		list.add(this.rating);
		return list;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

}
