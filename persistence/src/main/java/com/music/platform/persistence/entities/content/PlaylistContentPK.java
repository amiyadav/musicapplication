package com.music.platform.persistence.entities.content;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.music.platform.persistence.entities.baseentity.CompositeKey;

@Embeddable
public class PlaylistContentPK extends CompositeKey{

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="CLIENT_PLAYLIST_USER_PLAYLIST_ID")
	private ClientPlaylist clientPlaylist;
	
	@ManyToOne
	@JoinColumn(name="CONTENT_CONTENT_ID")
	private Content content;

	@Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.content);
		list.add(this.clientPlaylist);
		return list;
		
	}
	
	public ClientPlaylist getClientPlaylist() {
		return clientPlaylist;
	}

	public void setClientPlaylist(ClientPlaylist clientPlaylist) {
		this.clientPlaylist = clientPlaylist;
	}


	public Content getContent() {
		return content;
	}

	public void setContent(Content content) {
		this.content = content;
	}
}
