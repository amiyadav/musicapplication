package com.music.platform.persistence.entities.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.AuditableEntity;

@Entity
@Table(name = "USER_CHAT_PROFILE")
public class UserChatProfile extends AuditableEntity<UserMaster> implements Serializable {
	@Id
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "USER_MASTER_USER_ID")
	private UserMaster userMaster;
	
	@Column(name = "USER_CHAT_NAME")
	private String userChatName;
	
	@Column(name = "USER_SKYPE_ID")
	private String userSkypeId;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "USER_CHAT_STATUS_TYPE_USER_CHAT_STATUS_TYPE_ID")
	private UserChatStatusType userChatStatusId;

	@Override
	public UserMaster getId() {
		// TODO Auto-generated method stub
		return userMaster;
	}

	@Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.userChatName);
		list.add(this.userSkypeId);
		list.add(this.userChatStatusId);
		
		return list;
	}

	public String getUserChatName() {
		return userChatName;
	}

	public void setUserChatName(String userChatName) {
		this.userChatName = userChatName;
	}

	public String getUserSkypeId() {
		return userSkypeId;
	}

	public void setUserSkypeId(String userSkypeId) {
		this.userSkypeId = userSkypeId;
	}

	public UserChatStatusType getUserChatStatusId() {
		return userChatStatusId;
	}

	public void setUserChatStatusId(UserChatStatusType userChatStatusId) {
		this.userChatStatusId = userChatStatusId;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

}
