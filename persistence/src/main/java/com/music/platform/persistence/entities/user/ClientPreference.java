package com.music.platform.persistence.entities.user;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.AuditableEntity;

@Entity
@Table(name = "CLIENT_PREFERENCE")
public class ClientPreference extends AuditableEntity<Client> {

	@Id
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CLIENT_USER_MASTER_USER_ID")
	private Client client;
	
	@Column(name="IS_SYSTEM_GENERATED")
	private boolean IS_SYSTEM_GENERATED;
	
	@Column(name="LOGIN_PREFERENCE")
	private boolean LOGIN_PREFERENCE;
	
	@Override
	public Client getId() {
		// TODO Auto-generated method stub
		return client;
	}

	@Override
	public List<? extends Object> getCompareables() {
		List<Object> list = new ArrayList<Object>();
		list.add(this.client);
		
		return list;
	}
	
	public void setClient(Client client) {
		this.client = client;
	}

	public boolean isIS_SYSTEM_GENERATED() {
		return IS_SYSTEM_GENERATED;
	}

	public void setIS_SYSTEM_GENERATED(boolean iS_SYSTEM_GENERATED) {
		IS_SYSTEM_GENERATED = iS_SYSTEM_GENERATED;
	}

	public boolean isLOGIN_PREFERENCE() {
		return LOGIN_PREFERENCE;
	}

	public void setLOGIN_PREFERENCE(boolean lOGIN_PREFERENCE) {
		LOGIN_PREFERENCE = lOGIN_PREFERENCE;
	}

	}
