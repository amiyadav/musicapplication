package com.music.platform.persistence.entities.user;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.music.platform.persistence.entities.baseentity.CompositeKey;
import com.music.platform.persistence.entities.content.Content;
//Testing purpose
@Embeddable
public class UserContentSingerPK extends CompositeKey{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5841208745198482931L;

	@ManyToOne
	@JoinColumn(name = "CONTENT_CONTENT_ID")
	private Content content;
	
	@ManyToOne
	@JoinColumn(name = "USER_MASTER_USER_ID")
	private UserMaster userMaster;

	@Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.content);
		list.add(this.userMaster);
		
		return list;
	}

	public Content getContent() {
		return content;
	}

	public void setContent(Content content) {
		this.content = content;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

}