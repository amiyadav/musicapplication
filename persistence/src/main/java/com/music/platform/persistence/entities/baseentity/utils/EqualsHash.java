

package com.music.platform.persistence.entities.baseentity.utils;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;

public final class EqualsHash {
    
    private EqualsHash() { }

    /**
     * Implementation of {@link #equals(Object)}.
     * 
     * @param one
     *            IComparable
     * @param two
     *            Object
     * @return equals
     */
    public static boolean equals(final IComparable one, final Object two) {
        boolean equals = true;

        if (one == two) {
            equals = true;
        } else {
            if (one == null || two == null || !one.getClass().equals(two.getClass())) {
                equals = false;
            } else {
                Iterator<? extends Object> twos = ((IComparable) two).getCompareables().iterator();
                for (Object ones : one.getCompareables()) {
                    Object compareTo = twos.next();
                    if (ones != null && !ones.equals(compareTo)) {
                        equals = false;
                        break;
                    }
                }
            }
        }
        return equals;
    }


    public static int hashCode(final IComparable comparable) {
        int prime = 31;
        int result = 1;
        for (Object compare : comparable.getCompareables()) {
            int compareHash = 0;
            if (compare != null) {
                compareHash = compare.hashCode();
            }
            result = prime * result + compareHash;
        }
        return result;
    }


    @SuppressWarnings("unchecked")
    public static int compareTo(final IComparable one, final IComparable two) {
        int result = 0;
        if (!one.equals(two)) {
            List<? extends Object> twoComparables = two.getCompareables();
            Iterator<? extends Object> twoIter = twoComparables.iterator();
            for (Object compare : one.getCompareables()) {
                Object compareTo = twoIter.next();
                if (!ObjectUtils.equals(compare, compareTo)) {
                    if (compare instanceof Comparable<?>) {
                        // String.compare to null fails with a NullPointerException :-(
                        // => we have to check compareTo for null, compare can't be null
                        if (compareTo == null) {
                            result = 1;
                        } else {
                            result = ((Comparable) compare).compareTo(compareTo);
                        }
                    } else {
                        // String compare if types are not comparable
                        result =  String.valueOf(compare).compareTo(String.valueOf(compareTo));
                    }
                    if (result != 0) {
                        break;
                    }
                }
            }
        }
        return result;
    }

}
