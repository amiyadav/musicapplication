package com.music.platform.persistence.entities.order;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.music.platform.persistence.entities.baseentity.CompositeKey;
//Dummy just for testing
@Embeddable
public class OrderItemPK extends CompositeKey{

	@Id
	@GeneratedValue
	@Column(name = "ORDER_ITEMS_ID")
	private int orderItemsID ;
	
	@ManyToOne
	@JoinColumn(name = "ORDER_ORDER_ID")
	private Order order;
	
	@Override
	public List<? extends Object> getCompareables() {
		List<Object> list = new ArrayList<Object>();
		list.add(this.order);
		list.add(this.orderItemsID);
		return list;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public int getOrderItemsID() {
		return orderItemsID;
	}

	public void setOrderItemsID(int orderItemsID) {
		this.orderItemsID = orderItemsID;
	}

	
}
