package com.music.platform.persistence.entities.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PAYMENT_STATUS")
public class PaymentStatus {
	@Id
	@Enumerated(EnumType.STRING)
	@Column(name = "PAYMENT_STATUS_ID", updatable = false, nullable = false)
	private String paymentStatusID;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	public String getPaymentStatusID() {
		return paymentStatusID;
	}

	public void setPaymentStatusID(String paymentStatusID) {
		this.paymentStatusID = paymentStatusID;
	}

	

	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
