package com.music.platform.persistence.entities.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PAYMENT_TYPE")
public class PaymentType {
		@Id
		@Enumerated(EnumType.STRING)
		@Column(name="PAYMENT_TYPE_ID",updatable=false,nullable=false)
		private String paymentTypeID;
		
		@Column(name="DESCRIPTION")
		private String description;
		
		public String getPaymentTypeID() {
			return paymentTypeID;
		}

		public void setPaymentTypeID(String paymentTypeID) {
			this.paymentTypeID = paymentTypeID;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		
}
