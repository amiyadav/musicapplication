package com.music.platform.persistence.entities.transaction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.CompositeKey;

@Entity
@Table(name="PAYMENT_TRANSACTION")
public class PaymentTransaction implements Serializable  {
	
	

	/*@Id
	@Embedded
	private PaymentTransactionPK paymentTransaction;
	*/
	
	@Id
	@Enumerated
	@Column(name="PAYMENT_TRANSACTION_ID")
	private int paymentTransactionID;
	
	@ManyToOne
	@JoinColumn(name = "PAYMENT_TYPE_PAYMENT_TYPE_ID")
	private PaymentType paymentType;
	
	@ManyToOne
	@JoinColumn(name = "PAYMENT_STATUS_PAYMENT_STATUS_ID")
	private PaymentStatus paymentStatus;
	
	@Column(name="AMOUNt")
	private Double amount;
	
	/*@Override
	public List<? extends Object> getCompareables() {
		List<Object> list = new ArrayList<Object>();
		list.add(this.paymentTransactionID);
		list.add(this.amount);
		list.add(this.paymentType);
		list.add(this.paymentStatus);
		return list;
	}*/

	public int getPaymentTransactionID() {
		return paymentTransactionID;
	}

	public void setPaymentTransactionID(int paymentTransactionID) {
		this.paymentTransactionID = paymentTransactionID;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

}
