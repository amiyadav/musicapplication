package com.music.platform.persistence.entities.baseentity;
import java.io.Serializable;
import java.util.List;

import javax.persistence.MappedSuperclass;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.StandardToStringStyle;

import com.music.platform.persistence.entities.baseentity.utils.EqualsHash;
import com.music.platform.persistence.entities.baseentity.utils.IComparable;


@MappedSuperclass
public abstract class CompositeKey implements Serializable, IComparable {

    /**
     * 
     */
    private static final long serialVersionUID = -1707677751748286131L;

    @Override
    public String toString() {
        StandardToStringStyle stringStyle = new StandardToStringStyle();
        stringStyle.setUseIdentityHashCode(false);
        return ReflectionToStringBuilder.toString(this, stringStyle);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        return EqualsHash.equals(this, obj);
    }

    @Override
    public int hashCode() {
        return EqualsHash.hashCode(this);
    }

    /**
     * Gets entity element values for comparison
     * 
     * @return List
     * 
     */
    public abstract List<? extends Object> getCompareables();

}
