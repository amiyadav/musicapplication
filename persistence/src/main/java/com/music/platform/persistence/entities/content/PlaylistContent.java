package com.music.platform.persistence.entities.content;

import java.io.Serializable;
// import java.util.ArrayList;
import java.util.Date;
// import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PLAYLIST_CONTENT")
public class PlaylistContent implements Serializable{


	private static final long serialVersionUID = 1L;
	
	@Id
	@Embedded
	private PlaylistContentPK playlistContent;
	
	@Column(name="ADDED_DATE")
	private Date addedDate;

	public PlaylistContentPK getPlaylistContent() {
		return playlistContent;
	}

	public void setPlaylistContent(PlaylistContentPK playlistContent) {
		this.playlistContent = playlistContent;
	}

	public Date getDate() {
		return addedDate;
	}

	public void setDate(Date addedDate) {
		this.addedDate = addedDate;
	}
	
}
