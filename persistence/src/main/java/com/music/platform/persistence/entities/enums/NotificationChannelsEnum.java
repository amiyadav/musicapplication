package com.music.platform.persistence.entities.enums;

public enum NotificationChannelsEnum {
	
	EMAIL("EMAIL"),
	SMS("SMS");
	
	private String description;
	
	NotificationChannelsEnum(String desc){
		
		this.description = desc;
		
	}
	
	public String getDescription(){
		
		return description;
		
	}
	

}
