package com.music.platform.persistence.dao.basedao.bean;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.internal.SessionImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.music.platform.persistence.dao.basedao.BaseDAO;
import com.music.platform.persistence.dao.user.UserDAOBean;
import com.music.platform.persistence.entities.baseentity.AuditableEntity;
import com.music.platform.persistence.entities.baseentity.DataAccessEntity;

@Transactional
public abstract class BaseDAOBean implements BaseDAO {
	private static final Logger logger = LoggerFactory.getLogger(BaseDAOBean.class);
    private static final int MAX_FLUSH_COUNT = 50;
    
    @Autowired
    public SessionFactory sessionFactory;
    public abstract SessionFactory getSessionFactory();
    private Map<Session,Integer> batchCount = new HashMap<Session,Integer>();
    
    @SuppressWarnings("unchecked")
    public <T extends DataAccessEntity> void delete(T entity) {
        Session currentSession = getSessionFactory().getCurrentSession();
	
        currentSession.delete(entity);
        if (currentSession.getFlushMode().equals(FlushMode.AUTO)) {
			currentSession.flush();
//        } else {
//            batchFlush(currentSession);
        }
    }

    /**
     *{@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public <T extends AuditableEntity> T remove(T entity) {
        this.save(entity);
        return entity;
    }

    /**
     *{@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public <T extends DataAccessEntity, ID extends Serializable> T read(Class<T> aClass, ID id) {
        return (T) getSession().get(aClass, id);

    }

    /**
     *{@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public <T extends DataAccessEntity> T read(T entityInstance) {
        getSessionFactory().getCurrentSession().setCacheMode(CacheMode.GET);
        return (T) this.read(entityInstance.getClass(), entityInstance.getId());
    }

    /**
     * Flushes session
     */
    public void flush() {
        getSessionFactory().getCurrentSession().flush();
    }

    /**
     * Get session
     * 
     * @return Session
     */
    public Session getSession() {
        return getSessionFactory().getCurrentSession();
    }

    /**
     *{@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public <T extends DataAccessEntity> List<T> listAll(Class<T> c) {
        Criteria crit = getSession().createCriteria(c);
        return crit.list();
    }

    /**
     *{@inheritDoc}
     */
    public void setFlushMode(FlushMode flushMode) {
    	getSession().setFlushMode(flushMode);
    }
    
    /**
     *{@inheritDoc}
     */
    public FlushMode getFlushMode() {
    	return getSession().getFlushMode();
    }
    
    /**
     *{@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public <T extends DataAccessEntity> List<T> listEntities(T entityInstance) {
        Criteria crit = getSession().createCriteria(entityInstance.getClass());
        Example example = Example.create(entityInstance);
        crit.add(example);
        return crit.list();
    }



    /**
     *{@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public <T extends AuditableEntity> T save(T entity) {
    	if (entity.getId() == null) {
        	entity.setCreationDate(new Date());
        	entity.setUpdatedDate(new Date());
    	} else {
    		entity.setUpdatedDate(new Date());
    	}
        getSession().saveOrUpdate(entity);
        return entity;
    }

    /**
     *{@inheritDoc}
     */

    @SuppressWarnings("unchecked")
    private <T extends AuditableEntity> T sessionPersist(T entity) {
        getSession().persist(entity);
        return entity;
    }

    /**
     *{@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public <T extends AuditableEntity> T create(T entity) throws Exception{
        try {
        	System.out.print("$$$$$" + getSessionFactory().getCurrentSession());
            T returnEntity = save(entity);
            Session currentSession = getSessionFactory().getCurrentSession();
            if (currentSession.getFlushMode().equals(FlushMode.AUTO)) {
                currentSession.flush();
//            } else {
//                batchFlush(currentSession);
            }
            return returnEntity;
        } catch (ConstraintViolationException exception) {
            logger.debug("Encountered Constraint Violation during creation ", exception.getSQLException());
            throw new Exception();
        }
    }

    /**
     *{@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public <T extends AuditableEntity> T persist(T entity) throws Exception {
        try {
        	entity.setCreationDate(new Date());
        	entity.setUpdatedDate(new Date());
            T returnEntity = sessionPersist(entity);
            // getSessionFactory().getCurrentSession().flush();
			Session currentSession = getSessionFactory().getCurrentSession();

            // if (currentSession.getFlushMode().equals(FlushMode.MANUAL)) {
            //   batchFlush(currentSession);
            //}
            
            return returnEntity;
        } catch (ConstraintViolationException exception) {
            logger.debug("Encountered Constraint Violation during creation ", exception.getSQLException());
            throw new Exception();

        }
    }

    /**
     *{@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public <T extends AuditableEntity> T update(T entity) {
    	entity.setUpdatedDate(new Date());
		Session currentSession = getSessionFactory().getCurrentSession();
        currentSession.merge(entity);
        //if (currentSession.getFlushMode().equals(FlushMode.MANUAL)) {
        //    batchFlush(currentSession);
        //}
        return entity;
    }


    /**
     * Method which determines if the Boolean object is set
     * 
     * @param aBoolean
     *            <code>Boolean</code>
     * @return boolean
     */
    protected boolean isBooleanSet(Boolean aBoolean) {
        if (aBoolean == null) {
            return false;
        }
        return true;
    }

    /**
     * Method which determines if the Date object is set
     * 
     * @param aDate
     *            <code>Date</code>
     * @return boolean
     */
    protected boolean isDateSet(Date aDate) {
        if (aDate == null) {
            return false;
        }
        return true;
    }

    /**
     * Method which determines if the Double object is set
     * 
     * @param aDouble
     *            <code>Double</code>
     * @return boolean
     */
    protected boolean isDoubleSet(Double aDouble) {
        if (aDouble != null) {
            return true;
        }
        return false;
    }

    /**
     * Method which determines if the Float object is set
     * 
     * @param aFloat
     *            <code>Float</code>
     * @return boolean
     */
    protected boolean isFloatSet(Float aFloat) {
        if (aFloat != null) {
            return true;
        }
        return false;
    }

    /**
     * Method which determines if the Integer object is set
     * 
     * @param anInteger
     *            <code>Integer</code>
     * @return boolean
     */
    protected boolean isIntegerSet(Integer anInteger) {
        if (anInteger != null) {
            return true;
        }
        return false;
    }

    /**
     * Method which determines if the Short object is set
     * 
     * @param aShort
     *            <code>Short</code>
     * @return boolean
     */
    protected boolean isShortSet(Short aShort) {
        if (aShort != null) {
            return true;
        }
        return false;
    }

    /**
     * Method which determines if the Long object is set
     * 
     * @param aLong
     *            <code>Long</code>
     * @return boolean
     */
    protected boolean isLongSet(Long aLong) {
        if (aLong != null) {
            return true;
        }
        return false;
    }

    /**
     * Method which determines if the Object object is set
     * 
     * @param anObject
     *            <code>Object</code>
     * @return boolean
     */
    protected boolean isObjectSet(Object anObject) {
        if (anObject != null) {
            return true;
        }
        return false;
    }

    /**
     * Method which determines if the String object is set
     * 
     * @param aString
     *            <code>String</code>
     * @return boolean
     */
    protected boolean isStringSet(String aString) {
        if (aString == null) {
            return false;
        }
        return true;
    }



 

    /**
     *{@inheritDoc}
     */
    public void initBatchSession() {
        getSession().setFlushMode(FlushMode.MANUAL);
        batchCount.put(getSession(), 0);
    }
    
    private void batchFlush(Session currentSession) {
        
        if (batchCount.get(currentSession) == null) {
            batchCount.put(currentSession, 0);
        }
        int flushCount = batchCount.get(currentSession);
        if ( flushCount >= MAX_FLUSH_COUNT) {
            batchCount.put(currentSession, 0);
            getSession().flush();
            getSession().clear();
        } else {
            batchCount.put(currentSession, ++flushCount);
        }
    }

    /**
     *{@inheritDoc}
     */   
    public CallableStatement prepareCall(String query) throws SQLException {
    	Connection dbConnection = getConnection();
    	
    	return dbConnection.prepareCall(query);
    }
    
    /**
     *{@inheritDoc}
     */   
    public PreparedStatement prepareStatement(String query) throws SQLException {
    	Connection dbConnection = getConnection();
    	
    	return dbConnection.prepareStatement(query);
    }
    
    /**
     *{@inheritDoc}
     */   
    public Statement createStatement() throws SQLException {
    	Connection dbConnection = getConnection();
    	
    	return dbConnection.createStatement();
    }
    
    /**
     *{@inheritDoc}
     */   
    public void close(Object object) {
    	StringBuffer objectType = new StringBuffer();
    	
		if (object != null) {
			try {
				if (object instanceof Connection) {
					((Connection)object).close();
					objectType.append("DB connection");
				} else if (object instanceof Statement) {
					((Statement)object).close();
					objectType.append("DB statement");
				} else if (object instanceof ResultSet) {
					((ResultSet)object).close();
					objectType.append("result set");
				}
		    } catch (SQLException e) {
			    logger.warn("Failed to close " + objectType.toString(), e);
		    } finally {
			    object = null;
		    }
		}
    }
    
    
    /**
     *{@inheritDoc}
     */
    public Connection getConnection() {
    	return ((SessionImpl)getSession()).connection();
    }
    
    /**
     *{@inheritDoc}
     
    @Override
    public Connection getNativeConnection() throws SQLException {
        Connection dbConnection = null;
        
        dbConnection = BorrowedConnectionProxy.getWrappedConnection(getConnection());
        if (dbConnection.getClass().getName().startsWith("org.jboss")) {
                dbConnection = ((org.jboss.resource.adapter.jdbc.WrappedConnection) dbConnection)
                                .getUnderlyingConnection();
        }
    	
        return dbConnection;
    }
*/
    

}
