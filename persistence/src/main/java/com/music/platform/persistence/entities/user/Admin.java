package com.music.platform.persistence.entities.user;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import com.music.platform.persistence.entities.baseentity.DataAccessEntity;

@Entity
@Table(name = "ADMIN")
@PrimaryKeyJoinColumn(name="USER_MASTER_USER_ID" , referencedColumnName="USER_ID")
public class Admin extends UserMaster{

	public Admin() {
	}

	public Admin(UserMaster userMaster) {
		super(userMaster);
	}
	

}
