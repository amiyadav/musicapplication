package com.music.platform.persistence.entities.content;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.DataAccessEntity;
import com.music.platform.persistence.entities.enums.ContentLanguageEnum;

@Entity
@Table(name = "CONTENT_LANGUAGE")
public class ContentLanguage extends DataAccessEntity<ContentLanguageEnum> {

	private static final long serialVersionUID = 1L;
	
	@Id 
	@Enumerated(EnumType.STRING)
	@Column(name = "CONTENT_LANGUAGE_ID", updatable = false, nullable = false)
	private ContentLanguageEnum contentLanguageId;
	
	public ContentLanguageEnum getContentLanguageId() {
		return contentLanguageId;
	}

	public void setContentLanguageId(ContentLanguageEnum contentLanguageId) {
		this.contentLanguageId = contentLanguageId;
	}

	@Column(name = "DESCRIPTION")
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public ContentLanguageEnum getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		return null;
	}
}
