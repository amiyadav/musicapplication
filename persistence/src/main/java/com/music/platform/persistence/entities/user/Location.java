package com.music.platform.persistence.entities.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.AuditableEntity;

@Entity
@Table(name = "LOCATION")
public class Location extends AuditableEntity<Integer> implements Serializable{
	
	@Id
	@Column(name = "LOCATION_ID")
	@GeneratedValue
	private Integer locationId;
	
	@Column(name = "PINCODE")
	private String pinCode;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CITY_CITY_ID")
	private City city;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "STATE_STATE_ID")
	private State state;

	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return locationId;
	}

	@Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.pinCode);
		list.add(this.city);
		list.add(this.state);
		
		return list;
	}

	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public void setCity(City city) {
		this.city = city;
	}
}
