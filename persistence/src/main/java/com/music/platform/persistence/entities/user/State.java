package com.music.platform.persistence.entities.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.DataAccessEntity;

@Entity
@Table(name = "STATE")
public class State extends DataAccessEntity<Integer>  implements Serializable {
	@Id
	@Column(name = "STATE_ID")
	@GeneratedValue
	private Integer stateId;
	
	@Column(name = "STATE_NAME")
	private String stateName;
	

	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return stateId;
	}

	@Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.stateName);
		
		return list;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}
}
