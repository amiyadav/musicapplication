package com.music.platform.persistence.entities.transaction;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.music.platform.persistence.entities.baseentity.CompositeKey;
//Dummy just for checking
public class PaymentTransactionPK extends CompositeKey{

	@ManyToOne
	@JoinColumn(name = "PAYMENT_TYPE_PAYMENT_TYPE_ID")
	private PaymentType paymentType;
	
	@ManyToOne
	@JoinColumn(name = "PAYMENT_STATUS_PAYMENT_STATUS_ID")
	private PaymentStatus paymentStatus;
	
	@Override
	public List<? extends Object> getCompareables() {
		List<Object> list = new ArrayList<Object>();
		list.add(this.paymentType);
		list.add(this.paymentStatus);
		return list;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

}
