package com.music.platform.persistence.entities.enums;

public enum UserChatStatusEnum {
	ONLINE("ONLINE"),
	BUSY("BUSY"),
	OFFLINE("OFFLINE");
	

	private String description;
	
	UserChatStatusEnum(String desc){
		
		this.description = desc;
	}
	
	public String getDescription(){
		
		return description;
	}
	
}
