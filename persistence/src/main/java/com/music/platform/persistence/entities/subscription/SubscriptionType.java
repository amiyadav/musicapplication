package com.music.platform.persistence.entities.subscription;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.DataAccessEntity;
import com.music.platform.persistence.entities.enums.SubscriptionTypeEnum;


@Entity
@Table(name="SUBSCRIPTION_TYPE")
public class SubscriptionType extends DataAccessEntity<SubscriptionTypeEnum>{


	private static final long serialVersionUID = 1L;
	
	@Id
	@Enumerated(EnumType.STRING)
	@Column(name="SUBSCRIPTION_TYPE_ID")
	private SubscriptionTypeEnum subscriptionTypeId;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	public SubscriptionTypeEnum getSubscriptionTypeId() {
		return subscriptionTypeId;
	}

	public void setSubscriptionTypeId(SubscriptionTypeEnum subscriptionTypeId) {
		this.subscriptionTypeId = subscriptionTypeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public SubscriptionTypeEnum getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.description);
		return list;
	}

}
