package com.music.platform.persistence.entities.enums;

public enum ContentFormatTypeEnum {
	
	IMAGE("IMAGE"), 
	VIDEO("VIDEO"), 
	TEXT("TEXT"), 
	PDF("PDF"), 
	HTML("HTML"), 
	FLASH("HTML"),
	URL("URL");
	
	private String description;
	
	ContentFormatTypeEnum(String desc){
		
		this.description = desc;
		
	}
	
	public String getDescription(){
		
		return description;
	}
}
