package com.music.platform.persistence.entities.user;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.DataAccessEntity;
import com.music.platform.persistence.entities.enums.NotificationChannelsEnum;

@Entity
@Table(name = "NOTIFICATION_CHANNELS")

public class NotificationChannels extends DataAccessEntity<NotificationChannelsEnum> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Enumerated(EnumType.STRING)
	@Column(name = "NOTIFICATION_CHANNELS_ID")
	private NotificationChannelsEnum notificationChannelsId;
	

	@Column(name = "DESCRIPTION")
	private String description;

	public NotificationChannelsEnum getNotificationChannelsId() {
		return notificationChannelsId;
	}

	public void setNotificationChannelsId(NotificationChannelsEnum notificationChannelsId) {
		this.notificationChannelsId = notificationChannelsId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
//	@Override
	public NotificationChannelsEnum getId() {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
	public List<? extends Object> getCompareables() {
		List<Object> list = new ArrayList<Object>();
		list.add(this.description);
		
		return list;
	}
	

	

}
