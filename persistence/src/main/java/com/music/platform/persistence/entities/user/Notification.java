package com.music.platform.persistence.entities.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.AuditableEntity;

@Entity
@Table(name = "NOTIFICATION")
public class Notification extends AuditableEntity <Long> implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "NOTIFICATION_ID")
	private Long notificationId; 
	
	@Column(name = "RECEIVED_STATUS")
	private Boolean receivedStatus;
	
	@Column(name = "READ_STATUS")
	private Boolean readStatus;
	
	@Column(name = "NOTIFICATION_TIME")
	private Date notificationTime;
	
	@ManyToOne
	@JoinColumn(name = "USER_MASTER_USER_ID")
	private UserMaster userMaster;
	
	@ManyToOne
	@JoinColumn(name = "NOTIFICATION_CHANNELS_NOTIFICATION_CHANNELS_ID")
	private NotificationChannels notificationsChannel;
	
	@ManyToOne
	@JoinColumn(name = "NOTIFICATION_TYPE_NOTIFICATION_TYPE_ID")
	private NotificationType notificationType;

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<? extends Object> getCompareables() {
		List<Object> list = new ArrayList<Object>();
		list.add(this.receivedStatus);
		list.add(this.readStatus);
		list.add(this.notificationTime);
		list.add(this.notificationsChannel);
		list.add(this.notificationType);
		list.add(this.userMaster);
			
		return list;
	}

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public Boolean getReceivedStatus() {
		return receivedStatus;
	}

	public void setReceivedStatus(Boolean receivedStatus) {
		this.receivedStatus = receivedStatus;
	}

	public Boolean getReadStatus() {
		return readStatus;
	}

	public void setReadStatus(Boolean readStatus) {
		this.readStatus = readStatus;
	}

	public Date getNotificationTime() {
		return notificationTime;
	}

	public void setNotificationTime(Date notificationTime) {
		this.notificationTime = notificationTime;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public NotificationChannels getNotificationsChannel() {
		return notificationsChannel;
	}

	public void setNotificationsChannel(NotificationChannels notificationsChannel) {
		this.notificationsChannel = notificationsChannel;
	}

	public NotificationType getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}
	
	
	
}
