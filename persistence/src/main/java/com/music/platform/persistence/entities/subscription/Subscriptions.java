package com.music.platform.persistence.entities.subscription;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SUBSCRIPTIONS")
public class Subscriptions implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="SUBSCRIPTIONS_ID", updatable = false, nullable = false)
	private Integer subscriptionsId;
	
	@Column(name="IS_ACTIVE")
	private Boolean isActive;
	
//	@Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.isActive);
		return list;
		
	}

	public Integer getSubscriptionsId() {
		return subscriptionsId;
	}

	public void setSubscriptionsId(Integer subscriptionsId) {
		this.subscriptionsId = subscriptionsId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
}
