package com.music.platform.persistence.entities.content;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="CLIENT_PLAYLIST")
public class ClientPlaylist implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "CLIENT_PLAYLIST_ID")
	private Long clientPlaylistID;
	
	private String Name;
	
	private  String description;
	
	 @Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
}
