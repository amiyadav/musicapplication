package com.music.platform.persistence.entities.order;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.music.platform.persistence.entities.transaction.PaymentTransaction;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "ORDER_TRANSACTION_HISTORY")
public class OrderTransactionHistory {
	@ManyToOne
	@JoinColumn(name = "ORDER_ORDER__ID", referencedColumnName = "ORDER_ID")
	private Order order;
	
	@Id
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "PAYMENT_TRANSACTION_PAYMENT_TRANSACTION_ID")
	private PaymentTransaction paymentTransaction;

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public PaymentTransaction getPaymentTransaction() {
		return paymentTransaction;
	}

	public void setPaymentTransaction(PaymentTransaction paymentTransaction) {
		this.paymentTransaction = paymentTransaction;
	}
	
}
