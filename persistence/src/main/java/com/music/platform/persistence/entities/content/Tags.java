package com.music.platform.persistence.entities.content;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "TAGS")
public class Tags implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "TAGS_ID")
	@GeneratedValue
	private Long tagsID;

	@Column(name = "DESCRIPTION")
	private String description;
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	// @Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.tagsID);
		list.add(this.description);

		return list;
	}

	public Long getTagsID() {
		return tagsID;
	}

	public void setTagsID(Long tagsID) {
		this.tagsID = tagsID;
	}

}
