package com.music.platform.persistence.entities.user;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.DataAccessEntity;
import com.music.platform.persistence.entities.enums.NotificationChannelsEnum;
import com.music.platform.persistence.entities.enums.NotificationTypeEnum;

@Entity
@Table(name = "NOTIFICATION_TYPE")

public class NotificationType extends DataAccessEntity<NotificationTypeEnum> {
	
	
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Enumerated(EnumType.STRING)
	@Column(name = "NOTIFICATION_TYPE_ID")
	private NotificationChannelsEnum notificationType;
	
	@Column(name = "DESCRIPTION")
	private String description;

	public NotificationChannelsEnum getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(NotificationChannelsEnum notificationType) {
		this.notificationType = notificationType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

//	@Override
	public NotificationTypeEnum getId() {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
	public List<? extends Object> getCompareables() {
		List<Object> list = new ArrayList<Object>();
		list.add(this.description);
		
		return list;
	}

}