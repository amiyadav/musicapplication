package com.music.platform.persistence.entities.enums;

public enum UserTypeEnum {
	ADMIN("ADMIN"), 
	CLIENT("CLIENT"),
	CONSULTANTS("CONSULTANTS");

	private final String userCode;

	UserTypeEnum(String userCode) {
		this.userCode = userCode;
	}

	public String getuserCode() {
		return this.userCode;
	}
}
