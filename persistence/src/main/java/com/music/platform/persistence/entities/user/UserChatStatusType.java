package com.music.platform.persistence.entities.user;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.DataAccessEntity;
import com.music.platform.persistence.entities.enums.UserChatStatusEnum;

@Entity
@Table(name = "USER_CHAT_STATUS_TYPE")
public class UserChatStatusType extends DataAccessEntity<UserChatStatusEnum>{
	@Id
	@Enumerated(EnumType.STRING)
	@Column(name = "USER_CHAT_STATUS_TYPE_ID", updatable = false, nullable = false)
	private UserChatStatusEnum id;
	
	@Column(name = "DESCRIPTION")
	private String description;

	@Override
	public UserChatStatusEnum getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.description);
		
		return list;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(UserChatStatusEnum id) {
		this.id = id;
	}
}
