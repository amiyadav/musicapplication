package com.music.platform.persistence.entities.order;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.AuditableEntity;
import com.music.platform.persistence.entities.user.Client;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "ORDER")
public class Order extends AuditableEntity<Long> implements Serializable{
	
	@Id
	@Column(name = "ORDER_ID", updatable = false, nullable = false)
	@GeneratedValue
	private Long orderID;
	
	@ManyToOne
	@JoinColumn(name = "CLIENT_USER_MASTER_USER_ID", referencedColumnName = "USER_MASTER_USER_ID")
	private Client client;

	@Column(name="AMOUNT")
	private double amount;
	
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<? extends Object> getCompareables() {
		List<Object> list = new ArrayList<Object>();
		list.add(this.client);
		list.add(this.orderID);
		return list;
	}

	public Long getOrderID() {
		return orderID;
	}

	public void setOrderID(Long orderID) {
		this.orderID = orderID;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
}
