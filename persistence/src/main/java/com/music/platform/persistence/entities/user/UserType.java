package com.music.platform.persistence.entities.user;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.DataAccessEntity;
import com.music.platform.persistence.entities.enums.UserTypeEnum;

@Entity
@Table(name = "USER_TYPE")
public class UserType extends DataAccessEntity<UserTypeEnum> {
	@Id
	@Enumerated(EnumType.STRING)
	@Column(name = "USER_TYPE_ID", updatable = false, nullable = false)
	private UserTypeEnum userType;

	@Column(name = "DESCRIPTION")
	private String description;

	@Override
	public UserTypeEnum getId() {
		// TODO Auto-generated method stub
		return userType;
	}

	@Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.description);

		return list;
	}

	public void setUserType(UserTypeEnum userType) {
		this.userType = userType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public UserTypeEnum getUserType() {
		return userType;
	}

}
