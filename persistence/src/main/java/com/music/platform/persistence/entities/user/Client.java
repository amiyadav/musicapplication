package com.music.platform.persistence.entities.user;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.music.platform.persistence.entities.content.Content;
import com.music.platform.persistence.entities.subscription.SubscriptionItems;

/**
 * @author amityadav
 *
 */
@Entity
@Table(name = "CLIENT")
@PrimaryKeyJoinColumn(name = "USER_MASTER_USER_ID", referencedColumnName = "USER_ID")
public class Client  extends UserMaster {

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "CLIENT_FAV", joinColumns = { @JoinColumn(name = "CLIENT_USER_MASTER_USER_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "CONTENT_CONTENT_ID") })
	private Set<Content> contents = new HashSet<Content>();
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "SUBSCRIPTION_ITEMS_USER",
				joinColumns={@JoinColumn(name = "CLIENT_USER_MASTER_USER_ID")},
				inverseJoinColumns={@JoinColumn(name = "SUBSCRIPTION_ITEMS_SUBSCRIPTION_ITEMS_ID")})
	private Set<SubscriptionItems> subscriptionItems;
	
	 @Temporal(TemporalType.TIMESTAMP)
	private Date addeddDate;
	
	public Client() {
	}

	public Client(UserMaster userMaster) {
		super(userMaster);
	}

	public Set<Content> getContents() {
		return contents;
	}

	public void setContents(Set<Content> contents) {
		this.contents = contents;
	}

	public Date getAddeddDate() {
		return addeddDate;
	}

	public void setAddeddDate(Date addeddDate) {
		this.addeddDate = addeddDate;
	}

	public Set<SubscriptionItems> getSubscriptionItems() {
		return subscriptionItems;
	}

	public void setSubscriptionItems(Set<SubscriptionItems> subscriptionItems) {
		this.subscriptionItems = subscriptionItems;
	}
	

}
