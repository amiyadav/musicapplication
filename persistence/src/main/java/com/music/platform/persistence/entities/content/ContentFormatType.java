package com.music.platform.persistence.entities.content;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.DataAccessEntity;
import com.music.platform.persistence.entities.enums.ContentFormatTypeEnum;

@Entity
@Table(name ="CONTENT_FORMAT_TYPE")
public class ContentFormatType extends DataAccessEntity<ContentFormatTypeEnum>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2879699779565867863L;
	
	@Id
	@Enumerated(EnumType.STRING)
	@Column(name = "CONTENT_FORMAT_TYPE_ID", updatable = false, nullable = false)
	private ContentFormatTypeEnum id;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	

	@Override
	public ContentFormatTypeEnum getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.description);
		
		return list;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(ContentFormatTypeEnum id) {
		this.id = id;
	}

}
