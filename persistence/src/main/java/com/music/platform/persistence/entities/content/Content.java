package com.music.platform.persistence.entities.content;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.AuditableEntity;
import com.music.platform.persistence.entities.subscription.SubscriptionItems;

@Entity
@Table(name = "CONTENT")
public class Content extends AuditableEntity<Long> implements Serializable {

	private static final long serialVersionUID = 1167857004128610425L;

	@Id
	@GeneratedValue
	@Column(name = "CONTENT_ID")
	private Long contentId;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "CONTENT_TAG", joinColumns = { @JoinColumn(name = "CONTENT_CONTENT_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "TAGS_TAGS_ID") })
	private Set<Tags> tags;

	// * Should this be OneToMany or ManyToMany (This is many to many with one
	// extra table strategy to maintain relation b/w 2 entities)
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "SUBSCRIPTION_ITEM_CONTENT", joinColumns = {
			@JoinColumn(name = "CONTENT_CONTENT_ID") }, inverseJoinColumns = {
					@JoinColumn(name = "SUBSCRIPTION_ITEMS_SUBSCRIPTION_ITEMS_ID") })
	private Set<SubscriptionItems> subscriptionItems;

	@Column(name = "CONTENT_URI")
	private String conterntURI;

	@Column(name = "SIZE")
	private Float size;

	@Column(name = "IS_USER_CONTENT")
	private Boolean isUserContent;

	@Column(name = "DURATION")
	private Integer duration;

	@ManyToOne
	@JoinColumn(name = "CONTENT_FORMAT_TYPE_CONTENT_FORMAT_TYPE_ID")
	private ContentFormatType contentFormatType;

	@ManyToOne
	@JoinColumn(name = "CATAGORY_CATAGORY_ID")
	private Catagory catagory;

	@ManyToOne
	@JoinColumn(name = "CONTENT_LANGUAGE_CONTENT_LANGUAGE_ID")
	private ContentLanguage contentLanguage;

	@ManyToOne
	@JoinColumn(name = "CONTENT_STAGE_CONTENT_STAGE_ID")
	private ContentStage contentStage;

	@Column(name = "NAME")
	private String name;

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return contentId;
	}

	@Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.conterntURI);
		list.add(this.contentFormatType);
		list.add(this.catagory);
		list.add(this.contentLanguage);
		list.add(this.contentStage);
		return list;
	}

	public Long getContentId() {
		return contentId;
	}

	public void setContentId(Long contentId) {
		this.contentId = contentId;
	}

	public Set<Tags> getTags() {
		return tags;
	}

	public void setTags(Set<Tags> tags) {
		this.tags = tags;
	}

	public Set<SubscriptionItems> getSubscriptionItems() {
		return subscriptionItems;
	}

	public void setSubscriptionItems(Set<SubscriptionItems> subscriptionItems) {
		this.subscriptionItems = subscriptionItems;
	}

	public String getConterntURI() {
		return conterntURI;
	}

	public void setConterntURI(String conterntURI) {
		this.conterntURI = conterntURI;
	}

	public Float getSize() {
		return size;
	}

	public void setSize(Float size) {
		this.size = size;
	}

	public Boolean getIsUserContent() {
		return isUserContent;
	}

	public void setIsUserContent(Boolean isUserContent) {
		this.isUserContent = isUserContent;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public ContentFormatType getContentFormatType() {
		return contentFormatType;
	}

	public void setContentFormatType(ContentFormatType contentFormatType) {
		this.contentFormatType = contentFormatType;
	}

	public Catagory getCatagory() {
		return catagory;
	}

	public void setCatagory(Catagory catagory) {
		this.catagory = catagory;
	}

	public ContentLanguage getContentLanguage() {
		return contentLanguage;
	}

	public void setContentLanguage(ContentLanguage contentLanguage) {
		this.contentLanguage = contentLanguage;
	}

	public ContentStage getContentStage() {
		return contentStage;
	}

	public void setContentStage(ContentStage contentStage) {
		this.contentStage = contentStage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
