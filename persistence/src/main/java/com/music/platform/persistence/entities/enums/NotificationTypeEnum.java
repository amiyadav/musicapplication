package com.music.platform.persistence.entities.enums;

public enum NotificationTypeEnum {
	
	AA("AA"),
	BB("BB");
	
	private String description;
	
	NotificationTypeEnum(String desc){
		
		this.description = desc;
		
	}
	
	public String getDescription(){
		
		return description;
		
	}

}
