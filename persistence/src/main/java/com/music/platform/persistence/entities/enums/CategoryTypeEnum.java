package com.music.platform.persistence.entities.enums;

public enum CategoryTypeEnum {

	BHAJAN("BHAJAN");
	
	private String description;
	
	CategoryTypeEnum(String desc){
		
		this.description = desc;
		
	}
	
	public String getDescription(){
		
		return description;
	}
}
