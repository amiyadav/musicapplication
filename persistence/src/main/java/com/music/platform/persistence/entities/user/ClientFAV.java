package com.music.platform.persistence.entities.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.CompositeKey;
@Entity
@Table(name="CLIENT_FAV")
public class ClientFAV implements Serializable {
	@Id
	@Embedded
	private ClientContentPK clientContent;
	
	@Column(name="ADDED_DATE")
	private Date AddedDate;

	public ClientContentPK getClientContent() {
		return clientContent;
	}

	public void setClientContent(ClientContentPK clientContent) {
		this.clientContent = clientContent;
	}

	public Date getAddedDate() {
		return AddedDate;
	}

	public void setAddedDate(Date addedDate) {
		AddedDate = addedDate;
	}

	/*@Override
	public List<? extends Object> getCompareables() {
		List<Object> list = new ArrayList<Object>();
		list.add(this.clientContent);
		list.add(this.AddedDate);
		return list;
	}*/
}
