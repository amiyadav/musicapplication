package com.music.platform.persistence.entities.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.music.platform.persistence.entities.baseentity.DataAccessEntity;

@Entity
@Table(name = "CITY")
public class City extends DataAccessEntity<Integer> implements Serializable{

	@Id
	@Column(name = "CITY_ID")
	@GeneratedValue
	private Integer cityId;
	
	@Column(name = "CITY_NAME")
	private String cityName;
	

	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return cityId;
	}

	@Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.cityName);
		
		return list;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}
}
