

package com.music.platform.persistence.entities.baseentity.utils;

import java.util.List;



public interface IComparable {

    List<? extends Object> getCompareables();

}
