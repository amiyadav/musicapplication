package com.music.platform.persistence.entities.baseentity;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.MappedSuperclass;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.StandardToStringStyle;
import com.music.platform.persistence.entities.baseentity.utils.EqualsHash;
import com.music.platform.persistence.entities.baseentity.utils.IComparable;


@MappedSuperclass
public abstract class DataAccessEntity<ID extends Serializable> implements Serializable, IComparable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Return the identifier of the entity.
     * 
     * @return the identifier
     */
    public abstract ID getId();

    @Override
    public String toString() {
        StandardToStringStyle stringStyle = new StandardToStringStyle();
        stringStyle.setUseIdentityHashCode(false);
        ReflectionToStringBuilder reflectionToStringBuilder = new ReflectionToStringBuilder(this, stringStyle);
        List<String> excludeFields = getExcludedClassesFields();
        if(!excludeFields.isEmpty())
        {
            String [] excludeFieldsArray = {};
            reflectionToStringBuilder.setExcludeFieldNames(excludeFields.toArray(excludeFieldsArray));
        }
        return reflectionToStringBuilder.toString();
    }

    /**
     * This API returns the list of excluded fields which are type of Collection 
     * 
     * @param fields
     * @return
     */
    private List<String> getExcludedFields(Field[] fields)
    {
        List<String> excludeFields = new ArrayList<String>();
        for (Field field:fields){
            if (Collection.class.isAssignableFrom(field.getType())){
                excludeFields.add(field.getName());                
            }
        }
        return excludeFields;
    }
    
    /**
     * This API returns the list of excluded fields from child to all parent classes.
     * @return
     */
    @SuppressWarnings("unchecked")
    private List<String> getExcludedClassesFields()
    {
        Class clazz = this.getClass();
        List<String> excludeFields = getExcludedFields(clazz.getDeclaredFields());
        while (clazz.getSuperclass() != null ) {
            clazz = clazz.getSuperclass();
            excludeFields.addAll(getExcludedFields(clazz.getDeclaredFields()));
        }
        return excludeFields;
    }
    
   
    
    
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        return EqualsHash.equals(this, obj);
    }

    @Override
    public int hashCode() {
        return EqualsHash.hashCode(this);
    }

    /**
     * Gets entity element values for comparison
     * 
     * @return List
     * 
     */
    public abstract List<? extends Object> getCompareables();
}
