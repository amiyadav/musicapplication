package com.music.platform.persistence.entities.enums;

public enum SubscriptionTypeEnum {
	MONTHLY("MONTHLY"), 
	YEAR("YEAR");

	private final String subscriptionType;

	SubscriptionTypeEnum(String subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	public String geSubscriptionType() {
		return this.subscriptionType;
	}
}

