package com.music.platform.persistence.entities.enums;

public enum ContentLanguageEnum {
	ENGLISH("ENGLISH"), 
	HINDI("HINDI");

	private final String contentLanguage;

	ContentLanguageEnum(String contentLanguage) {
		this.contentLanguage = contentLanguage;
	}

	public String getContentLanguage() {
		return this.contentLanguage;
	}
}
