

package com.music.platform.persistence.dao.basedao;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.hibernate.FlushMode;

import com.music.platform.persistence.entities.baseentity.AuditableEntity;
import com.music.platform.persistence.entities.baseentity.DataAccessEntity;




public interface BaseDAO {

    /**
     * Deletes from database
     * 
     * @param entity
     *            <T>
     * 
     */
    @SuppressWarnings("unchecked")
    <T extends DataAccessEntity> void delete(T entity);

    /**
     * Remove
     * 
     * @param entity
     *            T
     * 
     * @return AuditableEntity T extends
     */
    @SuppressWarnings("unchecked")
    <T extends AuditableEntity> T remove(T entity);

    /**
     * Read based on Id
     * 
     * @param aClass
     *            Class<T>
     * @param id
     *            long
     * 
     * @return T extends BusinessEntity
     */
    @SuppressWarnings("unchecked")
    <T extends DataAccessEntity, ID extends Serializable> T read(Class<T> aClass, ID id);

    /**
     * Read based on Id
     * 
     * @param entityInstance
     *            T
     * 
     * @return T extends BusinessEntity
     */
    @SuppressWarnings("unchecked")
    <T extends DataAccessEntity> T read(T entityInstance);

    /**
     * List elements
     * 
     * @param aClass
     *            Class<T>
     * 
     * @return T extends BusinessEntity
     */
    @SuppressWarnings("unchecked")
    <T extends DataAccessEntity> List<T> listAll(Class<T> aClass);


    @SuppressWarnings("unchecked")
    <T extends DataAccessEntity> List<T> listEntities(T entityInstance);

    @SuppressWarnings("unchecked")
    <T extends AuditableEntity> T save(T entity);

 // throws exception till platform specific exception is not created
    @SuppressWarnings("unchecked")
    <T extends AuditableEntity> T create(T entity) throws Exception;


// throws exception till platform specific exception is not created
    @SuppressWarnings("unchecked")
    <T extends AuditableEntity> T persist(T entity) throws Exception;


    @SuppressWarnings("unchecked")
    <T extends AuditableEntity> T update(T entity);


    
    /**
     * Initialises the batch count
     * 
     */   
    public void initBatchSession();
    
    /**
     * configures the session flush mode
     * 
     */   
    public void setFlushMode(FlushMode flushMode);
    
    /**
     * returns current session flush mode
     * 
     */   
    public FlushMode getFlushMode();
    
    /**
     * Prepares and returns a callable statement based upon current underlying session
     * 
     * @param base query to create a call
     * 
     * @return prepared callable statement
     */   
    public CallableStatement prepareCall(String query) throws SQLException;
    
    /**
     * Prepares and returns a statement based upon current underlying session
     *
     * @param query base query to create statement
     * 
     * @return prepared statement
     */   
    public PreparedStatement prepareStatement(String query) throws SQLException;
    
    /**
     * Creates a statement based upon current underlying session
     * 
     * @return new basic JDBC statement
     */   
    public Statement createStatement() throws SQLException;
    
    /**
     * Closes the DB related object
     * 
     * @param DB object to close
     */   
    public void close(Object object);
    
    /**
     * Method returns underlying JDBC connection
     * 
     * @return underlying JDBC connection
     */
    public Connection getConnection();

    
    /**
     * Method returns underlying JDBC connection. This method should be used when it is required to cast java.sql.Connection 
     * object into native Oracle connection.
     * 
     * @return underlying JDBC connection
     
    public Connection getNativeConnection() throws SQLException;*/
}
