package com.music.platform.persistence.entities.order;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

@Entity
@Table(name="ORDER_ITEM")
public class OrderItems implements Serializable{
	
	/*@Id
	@Embedded
	private OrderItemPK orderItem;*/
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="ORDER_STATUS_ORDER_STATUS_ID")
	private OrderStatus orderStatus;
	
	@Id
	@GeneratedValue
	@Column(name = "ORDER_ITEMS_ID")
	private int orderItemsID ;
	
	@ManyToOne
	@JoinColumn(name = "ORDER_ORDER_ID")
	private Order order;
	
	@Column(name="AMOUNT")
	private Double amount;
	
	
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.orderStatus);
		list.add(this.amount);
		list.add(this.orderItemsID);
		list.add(this.order);
		return list;
	}


	public OrderStatus getOrderStatus() {
		return orderStatus;
	}


	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}


	public int getOrderItemsID() {
		return orderItemsID;
	}


	public void setOrderItemsID(int orderItemsID) {
		this.orderItemsID = orderItemsID;
	}


	public Order getOrder() {
		return order;
	}


	public void setOrder(Order order) {
		this.order = order;
	}


	public Double getAmount() {
		return amount;
	}


	public void setAmount(Double amount) {
		this.amount = amount;
	}


	
}
