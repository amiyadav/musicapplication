package com.music.platform.persistence.entities.user;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.music.platform.persistence.entities.content.PlaylistContent;

@Entity
@Table(name="CLIENT_PLAYLIST")
public class ClientPlaylist implements Serializable {
	
	/*@Id
	@Column(name = "CLIENT_PLAYLIST_ID")
	@GeneratedValue
	private Long clientPlaylistID;*/
	
	/*@Embedded
	@Id
	private ClientPlaylistPK clientPlaylist;*/
	
	/*@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "PLAYLIST_CONTENT", joinColumns = { @JoinColumn(name = "CLIENT_PLAYLIST_USER_PLAYLIST_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "CONTENT_CONTENT_ID") })
	private Set<Content> clientPlaylistContents = new HashSet<Content>();*/
	
	/*@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="CLIENT_USER_MASTER_USER_ID")
	private Client client;*/
	
	@ManyToOne
	@JoinColumn(name = "CLIENT_USER_MASTER_USER_ID")
	private Client client;
	
	@OneToMany
	@JoinColumn(name = "CLIENT_PLAYLIST_ID")
	private PlaylistContent playlistContent;
	
	@Column(name = "NAME")
	private String name;
	@Column(name = "DESCRIPTION")
	private String description;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public PlaylistContent getPlaylistContent() {
		return playlistContent;
	}
	public void setPlaylistContent(PlaylistContent playlistContent) {
		this.playlistContent = playlistContent;
	}
	
}
