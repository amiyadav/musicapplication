package com.music.platform.persistence.entities.user;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.music.platform.persistence.entities.baseentity.CompositeKey;
import com.music.platform.persistence.entities.content.PlaylistContent;
// Ambiguous just for testing purpose
@Embeddable
public class ClientPlaylistPK extends CompositeKey {

	@ManyToOne
	@JoinColumn(name = "CLIENT_USER_MASTER_USER_ID")
	private Client client;
	
	@OneToMany
	@JoinColumn(name = "CLIENT_PLAYLIST_ID")
	private PlaylistContent playlistContent;
	
	@Override
	public List<? extends Object> getCompareables() {
		List<Object> list = new ArrayList<Object>();
		list.add(this.client);
		list.add(this.playlistContent);
		return list;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public PlaylistContent getPlaylistContent() {
		return playlistContent;
	}

	public void setPlaylistContent(PlaylistContent playlistContent) {
		this.playlistContent = playlistContent;
	}

	

}
