package com.music.platform.persistence.entities.user;
//Testing purpose
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "USER_CONTENT_SINGER")
public class UserContentSinger implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Embedded
	private UserContentSingerPK userContentSinger;
	

	public UserContentSingerPK getUserContentSinger() {
		return userContentSinger;
	}

	public void setUserContentSinger(UserContentSingerPK userContentSinger) {
		this.userContentSinger = userContentSinger;
	}

//	@Override
	public List<? extends Object> getCompareables() {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<Object>();
		list.add(this.userContentSinger);
		
		return list;
	}

}
