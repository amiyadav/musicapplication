package com.music.platform.persistence;

import java.sql.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.AssertThrows;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.music.platform.persistence.dao.user.UserDAOBean;
import com.music.platform.persistence.entities.user.UserIdentity;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "/spring-persistence-test.xml", "/spring-persistence.xml"
})
@TestExecutionListeners( { DependencyInjectionTestExecutionListener.class })
@Transactional
public class UserTest{
	
	@Autowired
	private UserDAOBean userDAOBean;
	
	/*@Test
	public void validateLogin(){
		String user = "ADMIN";
		String pass = "ADMIN";
		try {
			List<UserIdentity> validateUserIdentityList = userDAOBean.getValidatedUserIdentity(user, pass);
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
		
	}*/
	@Test
	public void getValidatedUserIdentityTest(){
		
		try {
			List<UserIdentity> userlist = userDAOBean.getValidatedUserIdentity("aa", "bb");	
			Assert.assertTrue(userlist.size() == 0);
			java.util.Date date = new java.util.Date();
			date = DateUtils.addDays(date, 15);
			
			System.out.println("@@@@@@@@@@@@" + date);
		
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
			
	}
	
	/*@Test
	public void justfortestingTest1(){
		
		UserType userType = new UserType();
		userType.setUserType(UserTypeEnum.ADMIN);
		UserType userType1;
		userType1 = userDAOBean.read(userType);
		try {
			
			Assert.assertNotNull(userType1);
			Assert.assertTrue(userType1.getId().toString().equals("ADMIN"));
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
		
		UserMaster userMaster = new UserMaster();
		userMaster.setfName("fname");
		userMaster.setlName("lname");
		userMaster.setTitle("t");
		userMaster.seteMail1("email");
		
		Date date = new Date(12);
		userMaster.setDOB(date);
		userMaster.setMobile1("122342");
		userMaster.setProfilePicturePath("assddfsadf");
		userMaster.setGender("MALE");
		userMaster.setUserType(userType1);

		try {
			userDAOBean.create(userMaster);
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
		
		UserIdentity userIdentity = new UserIdentity();
		userIdentity.setIsActive(Boolean.TRUE);
		userIdentity.setUserName("TESTUSER");
		userIdentity.setPassword("Password");
		userIdentity.setId(userMaster);
		try {
			userDAOBean.create(userIdentity);
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
		
	}*/

	
	
}